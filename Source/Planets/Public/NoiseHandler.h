// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SimpleNoise.h"
#include "UObject/NoExportTypes.h"
#include "GenericPlatform/GenericPlatformMath.h"

#include "NoiseHandler.generated.h"


/**
 * 
 */
UCLASS(EditInlineNew)
class PLANETS_API UNoiseHandler : public UObject
{
	GENERATED_BODY()
private:
	UPROPERTY()
	USimpleNoise* Noise;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0.0", UIMax = "1.0"))
	float Strength;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0", UIMax = "32"))
	int Layers;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0.0", UIMax = "1.0"))
	float BaseFrequency;
	UPROPERTY(EditAnywhere, meta=(UIMin = "0.0", UIMax="10.0"))
	float Frequency;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0.0", UIMax = "10.0"))
	float Amplitude;
	UPROPERTY(EditAnywhere, meta = (UIMin = "0.0", UIMax = "20.0"))
	float Ground;

	float MaxElevation;
	float MinElevation;

	void RefreshMinMax(float value);

public:
	UNoiseHandler();
	
	void Reset();
	float Evaluate(const FVector point);

	USimpleNoise* GetNoise() const;
	const float GetMaxElevation() const { return MaxElevation; }

	const float GetMinElevation() const { return MinElevation; }
	void SetMinElevation(float value) { MinElevation = value; }

#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& e) override;
#endif
};
