// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GenericPlatform/GenericPlatform.h"
#include "UObject/NoExportTypes.h"
#include "Containers/UnrealString.h"

#include "GeometryUtils.h"

#include "HumbleSphereHelper.generated.h"

/*
*	Better resolution handler
*/

class FaceHandler
{
private:
	FVector LocalUp;
	FVector LocalRight;
	FVector LocalForward;

	TArray<FVector> Vertices;
	TArray<GeometryUtils::Triangle> Triangles;

	float Radius;
	int Resolution;
public:
	FaceHandler(int res, float radius, FVector localUp);

	const TArray<FVector>& GetVertices() const { return Vertices; }
	const TArray<GeometryUtils::Triangle>& GetTriangles() const { return Triangles; }
	const FVector* GetVerticesRaw() const { return Vertices.GetData(); }
	const int32* GetTrianglesRaw() const { return (int32*)Triangles.GetData(); }

	const FVector GetLocalUp() const { return LocalUp; }

	void CreateFace(int resolution, float radius);
	void Clear();
};
/**
 * 
 */

UCLASS(BlueprintType)
class PLANETS_API UHumbleSphereHelper : public UObject
{
	GENERATED_BODY()
protected:
	float Radius;
	int32 Resolution;
	TArray<FaceHandler> Faces;
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
public:
	UHumbleSphereHelper();

	FaceHandler* GetFace(int index) const
	{ 
		FaceHandler* facePtr = (FaceHandler*)Faces.GetData();
		return facePtr + index;
	}

	const TArray<FVector>& GetVertices()
	{
		Vertices.Reset(0);

		for (int i = 0; i < 6; ++i)
		{
			Vertices.Append(Faces[i].GetVertices());
		}

		return Vertices;
	}

	const TArray<int32>& GetTriangles()
	{
		Triangles.Reset(0);

		for (int i = 0; i < 6; ++i)
		{
			//Triangles.Append(TArray<int32>((int32*)Faces[i].GetTrianglesRaw()));
		}
		return Triangles;
	}

	void CreateSphere(int32 resolution, float radius);
	void Clear();

	void PostEditChangeProperty(struct FPropertyChangedEvent& e);
};
