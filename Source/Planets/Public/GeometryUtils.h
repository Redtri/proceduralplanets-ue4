// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GenericPlatform/GenericPlatform.h"
#include "Math/Vector.h"

#include "UObject/NoExportTypes.h"

namespace GeometryUtils
{
	static const TArray<FVector> cardinals =
	{
		FVector::ForwardVector, FVector::BackwardVector,
		FVector::UpVector, FVector::DownVector,
		FVector::RightVector, FVector::LeftVector
	};

	struct Triangle
	{
		int32 vert[3];
	};
}