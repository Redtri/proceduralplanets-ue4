// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "UObject/UObjectGlobals.h"
#include "PlanetAsset.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class PLANETS_API UPlanetAsset : public UDataAsset
{
	GENERATED_BODY()
public:
    UPlanetAsset()
    {
        UE_LOG(LogTemp, Warning, TEXT("Creating planet asset %d %f"), precision, radius);
    }

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float radius;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        int precision;

    void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent);
};
