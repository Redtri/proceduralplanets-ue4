// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "NoiseHandler.h"
#include "GeometryUtils.h"

#include "OctahedronHelper.generated.h"


namespace Octahedron
{

	static const FVector Vertices[] =
	{
		{ 0.f, 0.f, -1.f}, { 0.f, 1.f, 0.f}, { -1.f, 0.f, 0.f},
		{ 0.f, -1.f, 0.f}, { 1.f, 0.f, 0.f}, { 0.f, 0.f, 1.f}
		
	};

	static const GeometryUtils::Triangle Triangles[] =
	{
		{0,1,2}, {0,4,1}, {0,3,4}, {0,2,3},

		{5,2,1}, {5,3,2}, {5,4,3}, {5,1,4}
	};
}

/**
 * 
 */
UCLASS(BlueprintType)
class PLANETS_API UOctahedronHelper : public UObject
{
	GENERATED_BODY()
private:
	TArray<FVector> Vertices;
	TArray<FVector2D> UVs;
	TArray<GeometryUtils::Triangle> Triangles;

	TMap<int64, int> middlePointCache;
	int middlePointIndex;

	UPROPERTY(EditAnywhere, Instanced)
	UNoiseHandler* NoiseHandler;
	UPROPERTY(EditAnywhere, Instanced)
	TArray<UNoiseHandler*> Noises;

	float radius;

	void PlaceVertex(FVector& position);
protected:
	void Subdivide();
	int SplitEdge(int32 A, int32 B);

	int AddVertex(FVector& point);
public:
	UOctahedronHelper();

	const TArray<FVector>& GetVertices() const { return Vertices; }
	const TArray<GeometryUtils::Triangle>& GetTriangles() const { return Triangles; }
	const TArray<FVector2D>& GetUVs() const { return UVs; }

	const FVector* GetVerticesRaw() const { return Vertices.GetData(); }
	const int32* GetTrianglesRaw() const { return (int32*)Triangles.GetData(); }

	const UNoiseHandler* GetNoiseHandler() { return NoiseHandler; }

	void CreateOctasphere(uint8 precision, float sphereRadius);
	void Clear();
	void Normalize();
};