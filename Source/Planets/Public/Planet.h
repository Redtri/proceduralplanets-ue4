// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class UPlanetAsset;

#include "CoreMinimal.h"

#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "OctahedronHelper.h"
#include "HumbleSphereHelper.h"

#include "Containers/EnumAsByte.h"
#include "Delegates/IDelegateInstance.h"

#include "Planet.generated.h"

UENUM()
enum eSphereType
{
	HumbleCube, OctaSphere
};


UCLASS()
class PLANETS_API APlanet : public AActor
{
	GENERATED_BODY()
private:
	int GetMidPointIndex(TMap<int32, int32>& cache, int indexA, int indexB);
protected:
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FProcMeshTangent> Tangents;
	TArray<FVector2D> UVs;
	TArray<FColor> Colors;

	UPROPERTY(EditAnywhere)
	TEnumAsByte<eSphereType> typeOfSphere;

	UPROPERTY(EditAnywhere, Instanced)
	UOctahedronHelper *octahedronHelper;
	UPROPERTY(EditAnywhere, Instanced);
	UHumbleSphereHelper* humbleSphereHelper;

	UPROPERTY(VisibleAnywhere)
	UProceduralMeshComponent* ProceduralMesh;
	UMaterial* Material;
	UMaterialInstanceDynamic* DynamicMaterialInstance;

	FDelegateHandle DelegateHandle;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent& e) override;
#endif
public:
	UPROPERTY(EditAnywhere)
		UPlanetAsset* planetAssetBase;
	UPROPERTY(EditAnywhere, Instanced)
		UPlanetAsset* planetAsset;

	// Sets default values for this actor's properties
	APlanet();
	~APlanet();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Subdivide();
	void PostLoad();
	void PostActorCreated();
	void CreateSphere();
};
