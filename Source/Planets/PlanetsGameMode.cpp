// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlanetsGameMode.h"
#include "PlanetsPawn.h"

APlanetsGameMode::APlanetsGameMode()
{
	// set default pawn class to our flying pawn
	DefaultPawnClass = APlanetsPawn::StaticClass();
}
