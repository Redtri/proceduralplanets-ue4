// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlanetsGameMode.generated.h"

UCLASS(MinimalAPI)
class APlanetsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APlanetsGameMode();
};



