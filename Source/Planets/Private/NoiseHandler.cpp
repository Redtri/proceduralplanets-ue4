// Fill out your copyright notice in the Description page of Project Settings.


#include "NoiseHandler.h"

void UNoiseHandler::RefreshMinMax(float value)
{
	if (value > MaxElevation)
		MaxElevation = value;
	else if (value > 0.0f && value < MinElevation)
		MinElevation = value;
}

UNoiseHandler::UNoiseHandler()
{
	Noise = CreateDefaultSubobject<USimpleNoise>(TEXT("SimpleNoise"));
	Reset();
}

void UNoiseHandler::Reset()
{
	MaxElevation = 0.1f;
	MinElevation = 0.0f;
}

float UNoiseHandler::Evaluate(const FVector point)
{
	float value = 0.0f;
	float freq = BaseFrequency;
	float amplit = 1.0f;

	for (int i = 0; i < Layers; ++i)
	{
		float v = Noise->Evaluate(point * freq);
		value += ((v + 1) * 0.5f * amplit);// / (i + 1);

		freq *= Frequency;
		amplit *= Amplitude;
	}

	//UE_LOG(LogTemp, Warning, TEXT("%f"), value);
	value = FGenericPlatformMath::Max(0.0f, value - Ground) * Strength;
	RefreshMinMax(value);

	return value;
}

void UNoiseHandler::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	Super::PostEditChangeProperty(e);
}

USimpleNoise* UNoiseHandler::GetNoise() const
{
	return Noise;
}
