// Fill out your copyright notice in the Description page of Project Settings.


#include "OctahedronHelper.h"
#include "GenericPlatform/GenericPlatformMath.h"
#include "DrawDebugHelpers.h"

UOctahedronHelper::UOctahedronHelper()
{
	NoiseHandler = CreateDefaultSubobject<UNoiseHandler>(TEXT("NoiseHandler"));
}

void UOctahedronHelper::CreateOctasphere(uint8 precision, float sphereRadius)
{
	NoiseHandler->Reset();

	Vertices.Reset(0);
	Triangles.Reset(0);
	middlePointCache.Reset();

	middlePointIndex = 0;

	radius = sphereRadius;

	//UE_LOG(LogTemp, Warning, TEXT("Octasphere with %d and %f"), precision, sphereRadius);

	//Creating the basic octahedron
	for(auto baseVertex : Octahedron::Vertices)
	{
		AddVertex(baseVertex);
	}
	//Vertices.Append(Octahedron::Vertices, UE_ARRAY_COUNT(Octahedron::Vertices));
	Triangles.Append(Octahedron::Triangles, UE_ARRAY_COUNT(Octahedron::Triangles));
	//Normalizing for security
	Normalize();

	//UE_LOG(LogTemp, Error, TEXT("Subdividing operation %d"), precision);
	for(int i = 0; i < precision; ++i)
	{
		Subdivide();
	}
}

void UOctahedronHelper::Clear()
{
	Vertices.Reset(0);
	UVs.Reset(0);
	Triangles.Reset(0);
}

//Placing vertex considering its normalized position from the center, multiplied by noise and radius values
void UOctahedronHelper::PlaceVertex(FVector& position)
{
	float amplitude = (NoiseHandler->Evaluate(position) + 1) * 0.5f;
	position.Normalize();
	//UE_LOG(LogTemp, Warning, TEXT("%f"), (position * radius * amplitude).Size());
	position *= radius * amplitude;
}

void UOctahedronHelper::Subdivide()
{
	TArray<GeometryUtils::Triangle> newSphere;
	//newSphere.Reserve(Triangles.Num() * 3);

	//UE_LOG(LogTemp, Error, TEXT("Triangles %d"), Triangles.Num());
	for (auto&& triangle : Triangles)
	{
		//Creating or picking from cache, 3 vertices inside an existing triangle
		int a = SplitEdge(triangle.vert[0], triangle.vert[1]);
		int b = SplitEdge(triangle.vert[1], triangle.vert[2]);
		int c = SplitEdge(triangle.vert[2], triangle.vert[0]);

		//Adding the 4 new triangles inside the new sphere array
		newSphere.Add({triangle.vert[0], a, c});
		newSphere.Add({ triangle.vert[1], b, a });
		newSphere.Add({triangle.vert[2], c, b});
		newSphere.Add({a,b,c});

		GeometryUtils::Triangle tri1 = { triangle.vert[0], a, c };
		GeometryUtils::Triangle tri2 = { triangle.vert[1], b, a };
		GeometryUtils::Triangle tri3 = { triangle.vert[2], c, b };
		GeometryUtils::Triangle tri4 = { a,b,c };
	}
	Triangles = TArray<GeometryUtils::Triangle>(newSphere);

}

int UOctahedronHelper::SplitEdge(int32 A, int32 B)
{
	int64 min = FGenericPlatformMath::Min(A, B);
	int64 max = FGenericPlatformMath::Max(A, B);

	int64 key = (min << 32) + max;

	int* result = middlePointCache.Find(key);
	if (result) {
		//Vertex is in cache, we can return it
		return *(result);
	}

	FVector v1 = Vertices[A];
	FVector v2 = Vertices[B];

	FVector vMiddle = FVector(
		(v1.X + v2.X) / 2.f,
		(v1.Y + v2.Y) / 2.f,
		(v1.Z + v2.Z) / 2.f
	);

	//UE_LOG(LogTemp, Warning, TEXT("Not found in cache, creating : %s "), *vMiddle.ToString());
	//Adding vertex to the list of vertices
	int i = AddVertex(vMiddle);

	//Then adding it to the cache of new vertices
	middlePointCache.Add(key, i);

	return i;
}

void UOctahedronHelper::Normalize()
{
	FlushPersistentDebugLines(GetWorld());
	for (FVector& each : Vertices)
	{
		PlaceVertex(each);
	}
}

int UOctahedronHelper::AddVertex(FVector &point)
{
	PlaceVertex(point);
	Vertices.Add(point);

	return middlePointIndex++;
}
