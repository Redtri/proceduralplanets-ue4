// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetAsset.h"

void UPlanetAsset::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	FCoreUObjectDelegates::OnObjectPropertyChanged.Broadcast(this, PropertyChangedEvent);

	if (PropertyChangedEvent.ChangeType == EPropertyChangeType::Interactive)
		SnapshotTransactionBuffer(this);

	Super::PostEditChangeProperty(PropertyChangedEvent);

}
