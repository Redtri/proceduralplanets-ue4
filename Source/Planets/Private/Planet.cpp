// Fill out your copyright notice in the Description page of Project Settings.

#include "Planet.h"
#include "KismetProceduralMeshLibrary.h"
#include "Templates/UniquePtr.h"
#include "DrawDebugHelpers.h"
#include "PlanetAsset.h"

float epsilon = 0.000015f;

// Sets default values
APlanet::APlanet()
{

	//Initializing arrays
	Vertices.SetNum(0);
	Triangles.SetNum(0);
	Normals.SetNum(0);
	Tangents.SetNum(0);
	UVs.SetNum(0);
	Colors.SetNum(0);

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//Octasphere mesh
	ProceduralMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMesh"));
	ProceduralMesh->bUseAsyncCooking = true;

	static ConstructorHelpers::FObjectFinder<UMaterial> DefaultMat(TEXT("Material'/Game/Materials/SimplexNoise.SimplexNoise'"));

	if (DefaultMat.Object != NULL)
	{
		Material = (UMaterial*)DefaultMat.Object;
		DynamicMaterialInstance = UMaterialInstanceDynamic::Create(Material, this, "DynamicMaterial");
		UE_LOG(LogTemp, Warning, TEXT("Successfully found your default material. Dynamic instance created : %s"), *DynamicMaterialInstance->GetName());
	}

	octahedronHelper = CreateDefaultSubobject<UOctahedronHelper>(TEXT("OctahedronHelper"));
	humbleSphereHelper = CreateDefaultSubobject<UHumbleSphereHelper>(TEXT("HumblesphereHelper"));


	//UE_LOG(LogTemp, Warning, TEXT("Creating planet. Radius %.5f Precision %d"), radius, precision);
	//resolution = 10;
	/*DelegateHandle = FCoreUObjectDelegates::OnObjectPropertyChanged.AddLambda([this](UObject* Object, struct FPropertyChangedEvent& Event)
		{
			if (Object && planetAsset) {
				if (Object->GetFName().IsEqual(planetAsset->GetFName())) {
					CreateSphere();
				}
			}
			
		});
		*/
	CreateSphere();
}

APlanet::~APlanet()
{
	DelegateHandle.Reset();
}

// Called when the game starts or when spawned
void APlanet::BeginPlay()
{
	Super::BeginPlay();
	CreateSphere();
}

void APlanet::PostActorCreated()
{
	//UE_LOG(LogTemp, Warning, TEXT("Created"));
	CreateSphere();
	Super::PostActorCreated();
}

void APlanet::PostLoad()
{
	//UE_LOG(LogTemp, Warning, TEXT("Post load"));
	CreateSphere();
	Super::PostLoad();
}


void APlanet::CreateSphere()
{
	if(planetAsset)
	{
		FString propertyName = GET_MEMBER_NAME_CHECKED(APlanet, planetAsset).ToString();
		//UE_LOG(LogTemp, Warning, TEXT("PlanetAsset property name : %s"), *propertyName);
		//UE_LOG(LogTemp, Warning, TEXT("Planet class name : %s"), *APlanet::StaticClass()->GetFName().ToString());
		//UE_LOG(LogTemp, Warning, TEXT("Planet asset : %d %f"), planetAsset->precision, planetAsset->radius);

		Vertices.Reset(0);
		Triangles.Reset(0);
		//UE_LOG(LogTemp, Warning, TEXT("Type of sphere : %d. Humble : %d, Octa : %d"), typeOfSphere.GetValue(), eSphereType::HumbleCube, eSphereType::OctaSphere);

		switch (typeOfSphere.GetValue())
		{
			case eSphereType::HumbleCube:
			{
				octahedronHelper->Clear();
				humbleSphereHelper->CreateSphere(planetAsset->precision, planetAsset->radius);

				FaceHandler* currentFace = nullptr;

				for (int i = 0; i < 6; ++i)
				{
					currentFace = humbleSphereHelper->GetFace(i);

					Vertices = humbleSphereHelper->GetVertices();
					//Triangles = humbleSphereHelper->GetTriangles();
					
					for (int t = 0; t < currentFace->GetTriangles().Num(); ++t)
					{
						Triangles.Add(currentFace->GetTriangles()[t].vert[0] + i * planetAsset->precision * planetAsset->precision);
						Triangles.Add(currentFace->GetTriangles()[t].vert[1] + i * planetAsset->precision * planetAsset->precision);
						Triangles.Add(currentFace->GetTriangles()[t].vert[2] + i * planetAsset->precision * planetAsset->precision);
					}
					
					//Triangles.Append(TArray<int32>((int32*)currentFace->GetTrianglesRaw(), currentFace->GetTriangles().Num() * 3));
					Normals.Append(currentFace->GetVertices());
					UVs = TArray<FVector2D>{ {0.f, 0.f} };
				}
				UE_LOG(LogTemp, Warning, TEXT("%s's HumbleCube being created with %d triangles and %d vertices."), *GetFName().ToString(), Triangles.Num()/3, Vertices.Num());

			}
			break;
			case eSphereType::OctaSphere:
				humbleSphereHelper->Clear();
				octahedronHelper->CreateOctasphere(planetAsset->precision, planetAsset->radius);

				Vertices = TArray<FVector>(octahedronHelper->GetVertices());
				Triangles = TArray<int32>((int32*)octahedronHelper->GetTrianglesRaw(), octahedronHelper->GetTriangles().Num() * 3);
				Normals = TArray<FVector>(octahedronHelper->GetVertices());
				UVs = TArray<FVector2D>{ {0.f, 0.f} };

				UE_LOG(LogTemp, Warning, TEXT("%s's OctaSphere being created with %d triangles and %d vertices."), *GetFName().ToString(), Triangles.Num(), Vertices.Num());

				float minElevation = octahedronHelper->GetNoiseHandler()->GetMinElevation();
				float maxElevation = octahedronHelper->GetNoiseHandler()->GetMaxElevation();

				ProceduralMesh->SetMaterial(0, DynamicMaterialInstance);
				if (DynamicMaterialInstance)
				{
					DynamicMaterialInstance->SetVectorParameterValue("PlanetCenter", GetActorLocation());
					DynamicMaterialInstance->SetScalarParameterValue("PlanetRadius", planetAsset->radius);
					DynamicMaterialInstance->SetVectorParameterValue("MinMaxElevation", FLinearColor(minElevation, maxElevation, 0.0f, 0.0f));
					
					UE_LOG(LogTemp, Warning, TEXT("MinMax in mat : %s Max in RAM : %f"), *DynamicMaterialInstance->K2_GetVectorParameterValue("MinMaxElevation").ToString(), octahedronHelper->GetNoiseHandler()->GetMaxElevation());
				}

				break;
		}

		

		FlushPersistentDebugLines(GetWorld());
		for (int triIndex = 0; triIndex < Triangles.Num(); triIndex += 3)
		{
			FVector v1 = Vertices[Triangles[triIndex]];
			FVector v2 = Vertices[Triangles[triIndex + 1]];
			FVector v3 = Vertices[Triangles[triIndex + 2]];

			//DrawDebugSphere(GetWorld(), GetTransform().GetLocation() + v1, 10.f, 1, FColor::White, true, 20.f, SDPG_MAX);
			//DrawDebugSphere(GetWorld(), GetTransform().GetLocation() + v2, 10.f, 1, FColor::Yellow, true, 20.f, SDPG_MAX);
			//DrawDebugSphere(GetWorld(), GetTransform().GetLocation() + v3, 10.f, 1, FColor::Red, true, 20.f, SDPG_MAX);
			//UE_LOG(LogTemp, Warning, TEXT("Triangle %d :\n %s\n %s\n %s\n"), (triIndex/3), *v1.ToString(), *v2.ToString(), *v3.ToString());
			//DrawDebugLine(GetWorld(), GetTransform().GetLocation(), , FColor::Red, false, 10.f, 8, 5.f);
		}
		
		UKismetProceduralMeshLibrary::CalculateTangentsForMesh(Vertices, Triangles, UVs, Normals, Tangents);

		ProceduralMesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, Colors, Tangents, true);

		//Enable collision data
		ProceduralMesh->ContainsPhysicsTriMeshData(true);
	}
	else
		UE_LOG(LogTemp, Error, TEXT("Cannot create the planet sphere for %s planet actor. No PlanetAsset object assigned"), *GetFName().ToString());
}

#if WITH_EDITOR
void APlanet::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	Super::PostEditChangeProperty(e);
	//UE_LOG(LogTemp, Warning, TEXT(" Property changed : %s"), *e.GetPropertyName().ToString());
	//if(planetAsset)
		//UE_LOG(LogTemp, Warning, TEXT(" Planet asset infos : %d %f"), planetAsset->precision, planetAsset->radius);

	FName PropertyName = (e.Property != NULL) ? e.Property->GetFName() : NAME_None;
	
	if (PropertyName == GET_MEMBER_NAME_CHECKED(APlanet, planetAsset) ||
		PropertyName == GET_MEMBER_NAME_CHECKED(APlanet, octahedronHelper) ||
		PropertyName == GET_MEMBER_NAME_CHECKED(APlanet, typeOfSphere)
		)
	{
		CreateSphere();
	}

	if (PropertyName == GET_MEMBER_NAME_CHECKED(APlanet, planetAssetBase))
	{
		planetAsset = planetAssetBase;
		CreateSphere();
	}
}
#endif

void APlanet::Subdivide()
{
	TMap<int32, int32> midPointCache = TMap<int32, int32>();

	for (int i = 0; i < planetAsset->precision; ++i)
	{
		TArray<int32> newTriangles = TArray<int32>();
		for (int triIndex = 0; triIndex < Triangles.Num(); triIndex += 3)
		{
			int a = triIndex;
			int b = triIndex+1;
			int c = triIndex+2;

			int ab = GetMidPointIndex(midPointCache, a, b);
			int bc = GetMidPointIndex(midPointCache, b, c);
			int ca = GetMidPointIndex(midPointCache, c, a);

			TArray<int32> tmpNewTriangles = {
				a, ab, ca,
				b, bc, ab,
				c, ca, bc,
				ab, bc, ca
			};

			newTriangles.Append(tmpNewTriangles);
		}
		Triangles.Reset();
		Triangles.Append(newTriangles);
	}
}

int APlanet::GetMidPointIndex(TMap<int32, int32>& cache, int indexA, int indexB)
{
	int smallestIndex = FMath::Min(indexA, indexB);
	int greatestIndex = FMath::Max(indexA, indexB);
	int key = (smallestIndex << 16) + greatestIndex;
	
	int32 *result = cache.Find(key);

	if (result)
		return *result;
		
	FVector p1 = Vertices[indexA];
	FVector p2 = Vertices[indexB];
	FVector middle = FMath::Lerp(p1, p2, 0.5f);

	int32 result1 = Vertices.Num();
	Vertices.Add(middle);

	cache.Add(key, result1);

	return result1;
}

// Called every frame
void APlanet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

