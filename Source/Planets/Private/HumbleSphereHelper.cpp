// Fill out your copyright notice in the Description page of Project Settings.


#include "HumbleSphereHelper.h"


FaceHandler::FaceHandler(int res, float radius, FVector localUp)
{
	
	LocalUp = localUp;
	LocalRight = FVector(LocalUp.Y, LocalUp.Z, LocalUp.X);
	LocalForward = FVector::CrossProduct(LocalUp, LocalRight);

	Resolution = res;
	Radius = radius;
}

void FaceHandler::CreateFace(int resolution, float radius)
{
	using namespace GeometryUtils;

	//UE_LOG(LogTemp, Warning, TEXT("Localup : %s"), *LocalUp.ToString());

	Resolution = resolution;
	Radius = radius;

	//We may also just reset the array and add each vertex manually = Less optimized.
	Vertices.Reset(0);
	Vertices.SetNum(Resolution * Resolution);

	//The triangle class is already storing 3 sized arrays of integers.
	Triangles.Reset(0);
	Triangles.SetNum((Resolution - 1) * (Resolution - 1) * 2);

	int tIndex = 0;

	for (int y = 0; y < Resolution; ++y)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Row %d"), y);
		for (int x = 0; x < Resolution; ++x)
		{
			//Calculating the vertex index in the array. Same as incrementing for each x loop iteration.
			int vIndex = x + y * Resolution;
			//Storing triangle percents considering the resolution.
			FVector2D fillPercent = FVector2D(x, y) / (Resolution - 1);

			FVector point =
				LocalUp +
				LocalRight * (fillPercent.Y - 0.5f) * 2 +
				LocalForward * (fillPercent.X - 0.5f) * 2;
			point.Normalize();
			//UE_LOG(LogTemp, Warning, TEXT("Point %s "), *point.ToString());
			point *= Radius;
			//Adding the vertex in the array.
			Vertices[vIndex] = point;

			//For each vertex, we add the 2 bottom triangles associated.
			if (x != Resolution - 1 && y != Resolution - 1)
			{
				//Adding the triangles in the array.
				Triangles[tIndex] = { vIndex, vIndex + Resolution + 1, vIndex + Resolution };
				Triangles[tIndex + 1] = { vIndex, vIndex + 1, vIndex + Resolution + 1 };

				tIndex += 2;
			}
		}
	}
}

void FaceHandler::Clear()
{
	Vertices.Reset(0);
	Triangles.Reset(0);
}

/* 
* UHUMBLE SPHERE HELPER
*/

UHumbleSphereHelper::UHumbleSphereHelper()
{
}

void UHumbleSphereHelper::CreateSphere(int32 resolution, float radius)
{
	Resolution = resolution;
	Radius = radius;
	Faces.Reset(0);

	for (int i = 0; i < 6; ++i)
	{
		FaceHandler* currFaceHandler = nullptr;
		if (Faces.Num() < 6)
		{
			Faces.Add(FaceHandler(Resolution, Radius, GeometryUtils::cardinals[i]));
		}

		currFaceHandler = &Faces[i];

		currFaceHandler->CreateFace(Resolution, Radius);
	}
}

void UHumbleSphereHelper::Clear()
{
	if (Faces.Num() > 0)
	{
		for (int i = 0; i < 6; ++i)
		{
			Faces[i].Clear();
		}
	}
}

void UHumbleSphereHelper::PostEditChangeProperty(struct FPropertyChangedEvent& e)
{
	Super::PostEditChangeProperty(e);
	FName PropertyName = (e.Property != NULL) ? e.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(UHumbleSphereHelper, Resolution))
	{
		CreateSphere(Resolution, Radius);
	}
}