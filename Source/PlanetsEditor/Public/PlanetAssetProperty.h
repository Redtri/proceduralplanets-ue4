#pragma once

#include "IPropertyTypeCustomization.h"

#include "PropertyEditing.h"
#include "DetailWidgetRow.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SButton.h"

#include "Fonts/SlateFontInfo.h"
#include "InputCoreTypes.h"
#include "SlateBasics.h"
#include "SlateExtras.h"

#include "PlanetAsset.h"

#include "PropertyHandle.h"

class FPlanetAssetProperty : public IPropertyTypeCustomization
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

	virtual void CustomizeHeader(
		TSharedRef<IPropertyHandle> StructPropertyHandle,
		FDetailWidgetRow& HeaderRow,
		IPropertyTypeCustomizationUtils& StructCustomizationUtils
	) override;

	virtual void CustomizeChildren(
		TSharedRef<IPropertyHandle> StructPropertyHandle,
		IDetailChildrenBuilder& StructBuilder,
		IPropertyTypeCustomizationUtils& StructCustomizationUtils
	) override;
};