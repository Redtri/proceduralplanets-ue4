// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#if WITH_EDITOR

class APlanet;

#include "IDetailCustomization.h"

/**
 * 
 */
class FPlanetAssetDetails : public IDetailCustomization
{
private:
    APlanet* planet;
public:
    virtual void CustomizeDetails(IDetailLayoutBuilder& DetailBuilder) override;
    static TSharedRef <IDetailCustomization> MakeInstance();
    FReply RefreshPlanetSphere();
};

#endif
