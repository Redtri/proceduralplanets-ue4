// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class PlanetsEditor : ModuleRules
{
	public PlanetsEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ProceduralMeshComponent" } );
		PublicDependencyModuleNames.AddRange(new string[] { "Planets" } );

		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore"}) ;

		PublicIncludePaths.AddRange(
			new string[]
			{
				"PlanetsEditor/Public"
			});

		PrivateIncludePaths.AddRange(
			new string[]
            {
				"PlanetsEditor/Private"
            });
		
		/*
		PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		if(Target.bBuildEditor) {
			PrivateDependencyModuleNames.AddRange(new string[] { "DetailCustomizations", "PropertyEditor", "EditorStyle" });
			PublicDependencyModuleNames.AddRange(new string[] { "UnrealEd" });
        }
		*/
	}
}
