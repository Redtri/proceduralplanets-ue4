#include "PlanetAssetProperty.h"


#define LOCTEXT_NAMESPACE "PlanetsEditor"

void FPlanetAssetProperty::CustomizeHeader(TSharedRef<IPropertyHandle> StructPropertyHandle, FDetailWidgetRow& HeaderRow, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
    uint32 nbChildren = 0;
    StructPropertyHandle->GetNumChildren(nbChildren);
    FString propStrName = StructPropertyHandle->GetPropertyDisplayName().ToString();

    UE_LOG(LogTemp, Warning, TEXT("Customizing header's %s property. %d child handles detected."), *propStrName, nbChildren);
    if (nbChildren > 0)
    {
        for (uint32 i = 0; i < nbChildren; ++i)
        {
            FString handleStrValue;
            StructPropertyHandle->GetChildHandle(i)->GetValueAsDisplayString(handleStrValue);

            UE_LOG(LogTemp, Warning, TEXT("%d's header child : %s"), (i+1), *handleStrValue);
        }
    }
    /*
    HeaderRow.NameContent()
    [
        StructPropertyHandle->CreatePropertyNameWidget()
    ].ValueContent()
    [
        SNew(STextBlock)
        .Text(LOCTEXT("PropertyName","Your property has been replaced by plaintext!"))
    ];
    */
}

void FPlanetAssetProperty::CustomizeChildren( TSharedRef<IPropertyHandle> StructPropertyHandle, IDetailChildrenBuilder& StructBuilder, IPropertyTypeCustomizationUtils& StructCustomizationUtils)
{
    //FString propertyValue;
    //StructPropertyHandle->GetChildHandle(0)->GetValueAsDisplayString(propertyValue);
    UE_LOG(LogTemp, Warning, TEXT("Customizing Children."));// 1st child property handle : % s"), *propertyValue);
    //StructBuilder.;
    //.DisplayName(FText::FromString("Display name for custom row"));
    //do nothing
}

TSharedRef<IPropertyTypeCustomization> FPlanetAssetProperty::MakeInstance()
{
    return MakeShareable(new FPlanetAssetProperty);
}

#undef LOCTEXT_NAMEPSACE