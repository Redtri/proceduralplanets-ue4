// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetAssetDetails.h"

#include "PlanetsEditor.h"
#include "Planet.h"

#include "PropertyEditing.h"
#include "DetailWidgetRow.h"
#include "Widgets/Text/STextBlock.h"
#include "Widgets/Input/SButton.h"

#include "Fonts/SlateFontInfo.h"
#include "InputCoreTypes.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "Input/Reply.h"

#define LOCTEXT_NAMESPACE "PlanetsEditor"

TSharedRef<IDetailCustomization> FPlanetAssetDetails::MakeInstance()
{
	//UE_LOG(LogTemp, Warning, TEXT("Making instance"));
	return MakeShareable(new FPlanetAssetDetails);
}

void FPlanetAssetDetails::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	UE_LOG(LogTemp, Warning, TEXT("Customizing"));
	TSharedRef<IPropertyHandle> Prop = DetailBuilder.GetProperty(GET_MEMBER_NAME_CHECKED(APlanet, planetAsset));
	
	IDetailCategoryBuilder& Category = DetailBuilder.EditCategory("Planet", FText::GetEmpty(), ECategoryPriority::Important);

	APlanet* PlanetActor = nullptr;

	TArray<TWeakObjectPtr<UObject>> CustomizedObjects;
	DetailBuilder.GetObjectsBeingCustomized(CustomizedObjects);

	for (TWeakObjectPtr<UObject> Object : CustomizedObjects)
	{
		if (Object.IsValid())
		{
			PlanetActor = Cast<APlanet>(Object);
			if (!PlanetActor)
				break;
		}
	}

	//check(PlanetActor);
	planet = PlanetActor;

	Category.AddCustomRow(LOCTEXT("RowSearchName", "Magic"))
		.NameContent()
		[
			SNew(STextBlock)
			.Text(LOCTEXT("DetailName", "Regenerate planet"))
		].ValueContent()
		[
			SNew(SButton)
			.Text(LOCTEXT("ButtonText", "Regenerate planet"))
			.HAlign(HAlign_Center)
			.ToolTipText(LOCTEXT("ButtonToolTip", "Regenerate planet"))
			.OnClicked(this, &FPlanetAssetDetails::RefreshPlanetSphere)
		]
	;
}

FReply FPlanetAssetDetails::RefreshPlanetSphere()
{
	if (planet)
		planet->CreateSphere();
	return FReply::Handled();
}


#undef LOCTEXT_NAMEPSACE
