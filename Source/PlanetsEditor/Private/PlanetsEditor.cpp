#include "PlanetsEditor.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

#include "Planet.h"
#include "PlanetAssetDetails.h"
#include "PlanetAssetProperty.h"

IMPLEMENT_GAME_MODULE(FPlanetsEditorModule, PlanetsEditor);

#define LOCTEXT_NAMESPACE "PlanetsEditor"

void FPlanetsEditorModule::StartupModule()
{
    
    auto& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");

    UE_LOG(LogTemp, Warning, TEXT("PlanetsEditor: Log Started"));

    PropertyModule.RegisterCustomClassLayout(
        "Planet",//APlanet::StaticClass()->GetFName(),
        FOnGetDetailCustomizationInstance::CreateStatic(&FPlanetAssetDetails::MakeInstance)
    );
    /*
    PropertyModule.RegisterCustomPropertyTypeLayout(
        "planetAsset",
        FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FPlanetAssetProperty::MakeInstance)
    );
    */
    PropertyModule.NotifyCustomizationModuleChanged();
}

void FPlanetsEditorModule::ShutdownModule()
{
    UE_LOG(LogTemp, Warning, TEXT("PlanetsEditor: Log Ended"));

    if (FModuleManager::Get().IsModuleLoaded("PropertyEditor"))
    {
        auto& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
        PropertyModule.UnregisterCustomClassLayout("Planet");
        //PropertyModule.UnregisterCustomPropertyTypeLayout("planetAsset");
    }
}

#undef LOCTEXT_NAMESPACE